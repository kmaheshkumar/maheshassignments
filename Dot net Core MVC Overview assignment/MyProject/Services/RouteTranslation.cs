﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyProject.Services
{
    public class RouteTranslation
    {
        private static Dictionary<string, Dictionary<string, string>> Translations = new Dictionary<string, Dictionary<string, string>>
        {
            {
                "en", new Dictionary<string, string>
                {
                    { "entertainment", "Entertainment" },
                    { "bussiness", "Bussiness" },
                    {"account","Account" },
                    {"checkOutPage","CheckOutPage" }
                }
            },
            {
                "de", new Dictionary<string, string>
                {
                    { "unterhaltung", "Entertainment" },
                    { "geschäft", "Bussiness" },
                    {"flaggen" ,"Flags" }
                }
            },
        };

        public async Task<string> Resolve(string lang, string value)
        {
            var normalizedLang = lang.ToLowerInvariant();
            var normalizedValue = value.ToLowerInvariant();
            
            if (Translations.ContainsKey(normalizedLang) && Translations[normalizedLang].ContainsKey(normalizedValue))
            {
                var result = Translations[normalizedLang][normalizedValue];
                return result;
            }

            return null;
        }
    }
}
