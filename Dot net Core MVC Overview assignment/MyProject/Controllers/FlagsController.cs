﻿using Microsoft.AspNetCore.Mvc;

namespace MyProject.Controllers
{
    public class FlagsController : Controller
    {
        public ActionResult Entertainment()
        {
            ViewData ["Message"] = "this is the Entertainment Page";
            return View();
        }
        public ActionResult Bussiness()
        {
            ViewData["Message"] = "this is the Bussiness Page";
            return View();
        }
    }
}