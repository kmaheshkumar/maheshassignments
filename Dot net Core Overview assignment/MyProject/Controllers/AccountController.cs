﻿using Microsoft.AspNetCore.Mvc;
using MyProject.Models;

namespace Myapp.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(AccountModel acc)
        {
            var name = acc.Name;
            ViewBag.Message = @"WELCOME " + name + '!';
            if (name.Length > 0)
                return View();
            else
                return View("Error");
        }

        [HttpPost]
        public ActionResult CheckoutVerify()
        {
            ViewBag.Message = "Checkout Successful";
             return View("Index");
        }
        public ActionResult CheckOutPage()
        {
                return View();
        }
    }
}