﻿using Microsoft.EntityFrameworkCore;
using MOTC.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MOTC.Data.Extensions
{
    public static class MoviesBuilderExtension
    {
       public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MoviesModel>().HasData(
                new MoviesModel
                {
                    Id =1,
                    Name = "John Wick",
                    Genre = "Action",
                    ImagePath = "JohnWick.jpg"
                },
                new MoviesModel
                {
                    Id = 2,
                    Name = "Fall Out",
                    Genre = "Action",
                    ImagePath = "FallOut.jpg"
                },
                new MoviesModel
                {
                    Id =3,
                    Name= "Night Crawler",
                    Genre="Dark",
                    ImagePath = "NightCrawler.jpg"
                }
                );
        }
    }
}
