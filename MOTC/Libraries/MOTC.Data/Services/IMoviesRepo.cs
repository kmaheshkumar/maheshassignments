﻿using MOTC.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MOTC.Data.Services
{
    public interface IMoviesRepo
    {
        public IEnumerable<MoviesModel> GetAll();

        public MoviesModel Add(MoviesModel movie);
        public MoviesModel Update(MoviesModel movie);

        public MoviesModel GetById(int id);
        public MoviesModel GetByName(string Name);
        public IEnumerable<MoviesModel> GetByGenre(string Genre);

        public void Delete(int id);
    }
}
