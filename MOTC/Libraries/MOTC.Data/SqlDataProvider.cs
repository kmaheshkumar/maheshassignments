﻿using MOTC.Core.Models;
using MOTC.Data.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MOTC.Data
{
    public class SqlDataProvider : IMoviesRepo
    {
        private readonly AppDbContext _context;

        public SqlDataProvider(AppDbContext context)
        {
            _context = context;
        }

        public MoviesModel Add(MoviesModel movie)
        {
            _context.Movies.Add(movie);
            _context.SaveChanges();
            return movie;
        }

        public IEnumerable<MoviesModel> GetAll()
        {
            return _context.Movies;
        }

        public IEnumerable<MoviesModel> GetByGenre(string Genre)
        {
            if (!string.IsNullOrEmpty(Genre))
            {
                IEnumerable<MoviesModel> movieResult = _context.Movies.Where(
                                                         m => m.Genre == Genre
                                                         );
                return movieResult;
            }
            return null;
        }

        public MoviesModel GetById(int id)
        {
            var movieResult = _context.Movies.Find(id);
            if (movieResult != null)
            {
                return movieResult;
            }
            return null;
        }

        public MoviesModel GetByName(string Name)
        {
            if (!string.IsNullOrEmpty(Name))
            {
                var movieResult = _context.Movies.Where(
                                    m => m.Name == Name)
                                    .FirstOrDefault();
                return movieResult;
            }
            return null;
        }

        public MoviesModel Update(MoviesModel movie)
        {
            var movieResult = _context.Movies.Attach(movie);
            movieResult.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();
            return movie;
        }
        public void Delete(int id)
        {
            MoviesModel movieResult = _context.Movies.FirstOrDefault(
                                         x => x.Id == id
                                        );

            if (movieResult != null)
            {
                _context.Movies.Remove(movieResult);
                _context.SaveChanges();
            }
        }
    }
}
      
