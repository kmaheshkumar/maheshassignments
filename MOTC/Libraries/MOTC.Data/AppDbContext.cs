﻿using Microsoft.EntityFrameworkCore;
using MOTC.Core.Models;
using MOTC.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MOTC.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
        }

        public DbSet<MoviesModel> Movies { get; set; } 
    }
}
