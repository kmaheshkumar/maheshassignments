﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MOTC.Core.Models
{
    public class MoviesModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(15)]
        public string Genre { get; set; }

        [DefaultValue(null)]
        public string ImagePath { get; set; }
    }
}
