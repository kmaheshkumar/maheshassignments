#pragma checksum "C:\Users\KMaheshKumar\source\repos\maheshassignments\MOTC\MOTC\Views\Home\Category.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1b0e993c4c7bb6064da95c9ed6557b1343ed7830"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Category), @"mvc.1.0.view", @"/Views/Home/Category.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\KMaheshKumar\source\repos\maheshassignments\MOTC\MOTC\Views\_ViewImports.cshtml"
using MOTC;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\KMaheshKumar\source\repos\maheshassignments\MOTC\MOTC\Views\_ViewImports.cshtml"
using MOTC.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1b0e993c4c7bb6064da95c9ed6557b1343ed7830", @"/Views/Home/Category.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1fd5dc5d1ae950ff0714b5fccc71490ff0e5a660", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Category : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<MOTC.Core.Models.MoviesModel>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n<h3 style=\"text-align:center\">Category:");
#nullable restore
#line 6 "C:\Users\KMaheshKumar\source\repos\maheshassignments\MOTC\MOTC\Views\Home\Category.cshtml"
                                  Write(ViewData["Message"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h3>\r\n");
#nullable restore
#line 7 "C:\Users\KMaheshKumar\source\repos\maheshassignments\MOTC\MOTC\Views\Home\Category.cshtml"
 if (Model != null)
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <div>\r\n");
#nullable restore
#line 10 "C:\Users\KMaheshKumar\source\repos\maheshassignments\MOTC\MOTC\Views\Home\Category.cshtml"
         foreach (var item in Model)
        {
            

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\KMaheshKumar\source\repos\maheshassignments\MOTC\MOTC\Views\Home\Category.cshtml"
       Write(Html.Partial("_ImageGrid", item));

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\KMaheshKumar\source\repos\maheshassignments\MOTC\MOTC\Views\Home\Category.cshtml"
                                             ;
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </div>\r\n");
#nullable restore
#line 15 "C:\Users\KMaheshKumar\source\repos\maheshassignments\MOTC\MOTC\Views\Home\Category.cshtml"
}
else
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <h3>Result not Found</h3>\r\n");
#nullable restore
#line 19 "C:\Users\KMaheshKumar\source\repos\maheshassignments\MOTC\MOTC\Views\Home\Category.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<MOTC.Core.Models.MoviesModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
