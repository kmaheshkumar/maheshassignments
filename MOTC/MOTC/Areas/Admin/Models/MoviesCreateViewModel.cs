﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOTC.Areas.Admin.Models
{
    public class MoviesCreateViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Genre { get; set; }          
        
        public IFormFile Image { get; set; }
    }
}
