﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOTC.Areas.Admin.Models
{
    public class MoviesEditViewModel : MoviesCreateViewModel
    {
        public string ImagePath { get; set; }
    }
}
