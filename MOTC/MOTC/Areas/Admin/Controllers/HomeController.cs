﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MOTC.Areas.Admin.Models;
using MOTC.Core.Models;
using MOTC.Data.Services;

namespace MOTC.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class HomeController : Controller
    {
        private readonly IMoviesRepo _movieRepo;

        private readonly IHostingEnvironment _hostingEnvironment;

        public HomeController(IMoviesRepo movieRepo,
            IHostingEnvironment hostingEnvironment)
        {
            _movieRepo = movieRepo;
            _hostingEnvironment = hostingEnvironment;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Add(MoviesCreateViewModel movieFormModel)
        {
            if (ModelState.IsValid)
            {
                MoviesModel newMovieEntry = new MoviesModel
                {
                    Name = movieFormModel.Name,
                    Genre = movieFormModel.Genre
                };
               
                if (movieFormModel.Image != null)
                {
                    string imgRootPath = Path.Combine(_hostingEnvironment.WebRootPath,
                        "images\\movies");

                    string fileNameGen = Guid.NewGuid().ToString()
                        + "_"
                        + movieFormModel.Image.FileName;

                    string copyPath = Path.Combine(imgRootPath,  fileNameGen);

                    movieFormModel.Image.CopyTo(new FileStream(copyPath, FileMode.Create));

                    newMovieEntry.ImagePath = fileNameGen;
                }

                _movieRepo.Add(newMovieEntry);
                  return RedirectToAction("Display", new { id = newMovieEntry.Id } );
            }
            return View();
        }
        public IActionResult Edit()
        {
            return View();
        }
        public IActionResult EditPage(int id)
        {
            var movieResult = _movieRepo.GetById(id);
            if (movieResult != null)
            {
                MoviesEditViewModel movieEditView = new MoviesEditViewModel() {
                    Id = movieResult.Id,
                    Name = movieResult.Name,
                    Genre = movieResult.Genre,
                    ImagePath = movieResult.ImagePath
                };
                return View(movieEditView);
            }
            return View();
        }
        [HttpGet]
        public IActionResult Delete()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Delete(int Id)
        {
            return RedirectToAction("Display" , new { id = Id } );
        }
        public IActionResult Display(int? id)
        {
            if (id != null)
            {
                ViewBag.Message = id;
                var movieResult = _movieRepo.GetById(id.Value);
                if (movieResult != null)
                {
                    IEnumerable<MoviesModel> movieResultEnum = new[] { movieResult };
                    return View(movieResultEnum);
                }
                else
                    return View();
            }
            var model = _movieRepo.GetAll();
            return View(model);
        }
        public IActionResult SearchByName(string Name)
        {
            if (Name != null)
            {
                var movieResult = _movieRepo.GetByName(Name);
                if (movieResult != null)
                {
                    return RedirectToAction("Display", new { id = movieResult.Id });
                }
                else
                    return RedirectToAction("Display");
            }
            var model = _movieRepo.GetAll();
            return View(model);
        }
        public IActionResult Update(MoviesEditViewModel movieEditModel)
        {
            if (ModelState.IsValid)
            {
                MoviesModel newMovieEntry = new MoviesModel
                {
                    Name = movieEditModel.Name,
                    Genre = movieEditModel.Genre,
                    Id = movieEditModel.Id
                };

                if (movieEditModel.Image != null)
                {
                    string imgRootPath = Path.Combine(_hostingEnvironment.WebRootPath,
                        "images\\movies");

                    string fileNameGen = Guid.NewGuid().ToString()
                        + "_"
                        + movieEditModel.Image.FileName;

                    string copyPath = Path.Combine(imgRootPath,
                        fileNameGen);

                    movieEditModel.Image.CopyTo(new FileStream(copyPath, FileMode.Create));

                    newMovieEntry.ImagePath = fileNameGen;
                }
                else
                {
                    newMovieEntry.ImagePath = movieEditModel.ImagePath;
                }
                _movieRepo.Update(newMovieEntry);
                return RedirectToAction("Display", new { id = newMovieEntry.Id });
            }
            return View();
        }

        public IActionResult DeleteMovie(int id)
        {
            if (id != null)
            {
                 _movieRepo.Delete(id);
                return RedirectToAction("Display");
            }
            return RedirectToAction("Display");
        }
    }
}