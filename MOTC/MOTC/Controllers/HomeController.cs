﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MOTC.Data.Services;
using MOTC.Models;

namespace MOTC.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMoviesRepo _movieRepo;

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger , IMoviesRepo movieRepo)
        {
            _logger = logger;
            _movieRepo = movieRepo;
        }

        public IActionResult Index()
        {
            var model = _movieRepo.GetAll();
            return View(model);
        }

        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(AccountModel acc)
        {
            var name = acc.Name;
            var model = _movieRepo.GetAll();
            ViewBag.Message = @"WELCOME " + name + '!';
            if (name.Length > 0)
                return View(model);
            else
                return View("Error");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Category(string genre)
        {
            ViewData["Message"] = genre;
            if (genre != null)
            {
                var model = _movieRepo.GetByGenre(genre);
                return View(model);
            }
            return View(null);
        }
        

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
