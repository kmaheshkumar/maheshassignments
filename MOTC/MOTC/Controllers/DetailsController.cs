﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MOTC.Data.Services;

namespace MOTC.Controllers
{
    public class DetailsController : Controller
    {
        private readonly IMoviesRepo _movieRepo;

        public DetailsController(IMoviesRepo movieRepo)
        {
            _movieRepo = movieRepo;
        }
        public IActionResult MovieDetails(int Id)
        {
            var movieResult = _movieRepo.GetById(Id);
            if (movieResult != null)
            {
                return View(movieResult);
            }
                return View(null);
        }
    }
}
